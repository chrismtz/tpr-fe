import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { MdModule } from '../md/md.module';
import { HttpClientModule } from '@angular/common/http';

import { ReglasRoutes } from './reglas.routing'
import { ReglasListComponent } from './reglas-list.component';
import { ReglasService } from './reglas.service';
import { ReglasCreateComponent } from './reglas-create.component';
import { ReglasEditarComponent } from './reglas-editar.component';
import { MensajeDisplayModule } from '../shared/mensaje-display/mensaje-display.module';

@NgModule({
    imports:[
        CommonModule,
        RouterModule.forChild(ReglasRoutes),
        FormsModule,
        MdModule,
        HttpClientModule,
        MaterialModule,
        ReactiveFormsModule,
        MensajeDisplayModule
    ],
    declarations: [
        ReglasListComponent,
        ReglasCreateComponent,
        ReglasEditarComponent,
    ],
    providers: [ ReglasService ]
})

export class ReglasModule{}
