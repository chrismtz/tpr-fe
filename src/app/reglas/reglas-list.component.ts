import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReglasService } from './reglas.service';
import { Reglas, Data } from './reglas';

@Component({
    selector: 'app-reglas-lis',
    templateUrl: './reglas-list.component.html'
})

export class ReglasListComponent implements OnInit{
    reglas: Reglas[]
    total: number;
    cantidadPagina = 5;
    paginas: number[] = [];
    actual = 0;
    hasError = false
    errorMessage = ""

    constructor(private reglasService: ReglasService, private router: Router){}

    ngOnInit(){
        this.loadData()
    }

    loadData(page: number = 0){
        this.reglasService.listarReglas(page, this.cantidadPagina).subscribe(
            data => {
                console.log(data.message)
                var datos = data.data as Data
                this.reglas = datos.reglas
                this.total = datos.total
                let page_size = Math.ceil(this.total / this.cantidadPagina);
                this.paginas = new Array(page_size);

            },
            error => {
                console.error(error)
                this.hasError = true
                this.errorMessage = error.message
            }
        )
    }

    pagesLoad(page: number){
        console.log(page)
        this.actual = page
        this.loadData(page)
    }

    crearRegla(){
        this.router.navigate(['reglas/crear']);
    }

    editarRegla(id: number){
        this.router.navigate(['reglas/editar', id])
    }

    isError(){
        if(this.hasError){
          return true;
        }else{
          return false;
        }
    }

}