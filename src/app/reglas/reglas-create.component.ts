import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ReglasService } from './reglas.service';
import { Reglas, Data, Dato } from './reglas';
import { error } from 'protractor';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class ReglaStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}


@Component({
    selector: 'app-reglas-create',
    templateUrl: './reglas-create.component.html'
})

export class ReglasCreateComponent implements OnInit{
    regla : Reglas = new Reglas()
    submitted = false
    hasError = false
    errorMessage = ""

    validmontoEquivalenciaType: boolean = false;
    validlimiteInferiorType: boolean = false;
    validlimiteSuperiorType: boolean = false;
    matcher = new ReglaStateMatcher();
    type: FormGroup

    

    constructor(private reglasService: ReglasService, private router: Router, private formBuilder: FormBuilder){}

    ngOnInit(){
        this.type = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            montoEquivalencia: [null, Validators.required],
            limiteInferior: [null, Validators.required],
            limiteSuperior: [null, Validators.required],
        });

    }

    save(){
        this.reglasService.crearRegla(this.regla).subscribe(
            data => {
                console.log(data);
                this.submitted = true;
                //this.mensaje = "Se ha agregado la regla correctamente.";
            },
            error => {
                console.log(error)
                this.errorMessage = "No se ha podido agregar la nueva regla.";
                this.hasError = true;
              }
        
        );
        this.regla = new Reglas();

    }

    onSubmit(){
        if (this.type.valid){
            this.regla = this.type.value;
            console.log(this.type);
            console.log(this.regla);
            console.log(this.submitted);
            this.save();
        }else{
            this.validateAllFormFields(this.type);
        }

    }

    gotoList() {
        this.router.navigate(['reglas/lista']);
    }

    isFieldValid(form: FormGroup, field: string){
        return !form.get(field).valid && form.get(field).touched;
    }

    displayFieldCss(form: FormGroup, field: string){
        return {
            'has-error': this.isFieldValid(form,field),
            'has-feedback': this.isFieldValid(form,field)
        };
    }

    onType(){
        if (this.type.valid){

        }else{
            this.validateAllFormFields(this.type);
        }
    }

    validateAllFormFields(formGroup: FormGroup){
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if(control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }else if ( control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    montoEquivalenciaValidationType(e){
        if (e) {
            this.validmontoEquivalenciaType = true;
        }else {
            this.validmontoEquivalenciaType = false;
        }
    }

    limiteInferiorValidationType(e) {
        if (e) {
            this.validlimiteInferiorType = true;
        }else {
            this.validlimiteInferiorType = false;
        }
    }

    limiteSuperiorValidationType(e) {
        if (e) {
            this.validlimiteSuperiorType = true;
        }else {
            this.validlimiteSuperiorType = false;
        }
    }



}