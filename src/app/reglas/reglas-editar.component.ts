import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Reglas, Dato } from './reglas';
import { ReglasService } from './reglas.service';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class ReglaStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'app-reglas-edit',
    templateUrl: './reglas-create.component.html'
})

export class ReglasEditarComponent implements OnInit{

    regla = new Reglas();
    regla2 = new Reglas();
    param;
    id: number;
    submitted = false;
    hasError = false;
    errorMessage = "";
    
    validmontoEquivalenciaType: boolean = false;
    validlimiteInferiorType: boolean = false;
    validlimiteSuperiorType: boolean = false;
    matcher = new ReglaStateMatcher();
    type: FormGroup

    constructor(private reglasService: ReglasService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder){}

    ngOnInit(){
        this.type = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            montoEquivalencia: [null, Validators.required],
            limiteInferior: [null, Validators.required],
            limiteSuperior: [null, Validators.required],
        });
        this.id = +this.route.snapshot.paramMap.get('id')
        this.reglasService.getRegla(this.id).subscribe(
            data =>{
                this.regla = data
            },
            error =>{
                console.log(error)
                this.hasError = true
                this.errorMessage = error.message
            }
        )
    }

    gotoList(){
        this.router.navigate(['reglas/lista']);
    }

    save(){
        this.reglasService.editarRegla(this.regla).subscribe(
            data => {
                console.log(data);
                this.submitted = true;
                //this.mensaje = "Se ha agregado la regla correctamente.";
            },
            error => {
                console.log(error)
                this.errorMessage = "No se ha podido agregar la nueva regla.";
                this.hasError = true;
              }
        
        );
        this.regla = new Reglas();

    }

    onSubmit(){
        if (this.type.valid){
            this.regla2 = this.type.value;
            this.param = Object.values(this.regla2);
            this.regla.montoEquivalencia = this.param[0];
            this.regla.limiteInferior = this.param[1];
            this.regla.limiteSuperior = this.param[2];
            console.log(this.type);
            console.log(this.regla);
            console.log(this.submitted);
            this.save();
        }else{
            this.validateAllFormFields(this.type);
        }
    }

    isFieldValid(form: FormGroup, field: string){
        return !form.get(field).valid && form.get(field).touched;
    }

    displayFieldCss(form: FormGroup, field: string){
        return {
            'has-error': this.isFieldValid(form,field),
            'has-feedback': this.isFieldValid(form,field)
        };
    }

    onType(){
        if (this.type.valid){

        }else{
            this.validateAllFormFields(this.type);
        }
    }

    validateAllFormFields(formGroup: FormGroup){
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if(control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }else if ( control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    montoEquivalenciaValidationType(e){
        if (e) {
            this.validmontoEquivalenciaType = true;
        }else {
            this.validmontoEquivalenciaType = false;
        }
    }

    limiteInferiorValidationType(e) {
        if (e) {
            this.validlimiteInferiorType = true;
        }else {
            this.validlimiteInferiorType = false;
        }
    }

    limiteSuperiorValidationType(e) {
        if (e) {
            this.validlimiteSuperiorType = true;
        }else {
            this.validlimiteSuperiorType = false;
        }
    }

}
