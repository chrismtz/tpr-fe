export class Reglas {
    id: number
    limiteInferior: number
    limiteSuperior: number
    montoEquivalencia: number
}

export class DataAll{
    reglas: Reglas[]
}

export class Data{
    reglas: Reglas[]
    total: number
}

export class Dato{
    reglas: Reglas
}

export class JsonReglas{
    data: Data | DataAll | Dato
    status: number
    message: String
}

export class Paginate{
    startIndexPage: number
    pageSize: number
}
