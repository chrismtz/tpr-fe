import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { JsonReglas, Paginate, Reglas } from './reglas';

@Injectable()

export class ReglasService {

    private api = 'http://gy7228.myfoscam.org:8080/rest/reglas'

    constructor( private http: HttpClient ) { }

    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };

    listarReglas(page: number = 0, cant: number = 5): Observable<JsonReglas>{
        var body = new Paginate()
        body.pageSize = cant
        body.startIndexPage = page + 1
        return this.http.post<JsonReglas>(`${this.api}/getByPage`, body, this.httpOptions)
    }

    crearRegla(reglas: Reglas):Observable<JsonReglas>{
        return this.http.post<JsonReglas>(`${this.api}/add`, reglas, this.httpOptions)
    }

    getRegla(id: number): Observable <Reglas>{
        return this.http.get<Reglas>(`${this.api}/id/${id}`)
    }

    editarRegla(regla: Reglas): Observable<JsonReglas>{
        return this.http.put<JsonReglas>(`${this.api}/edit/${regla.id}`, regla)
    }

}