import {Routes} from '@angular/router';
import { ReglasListComponent } from './reglas-list.component';
import { ReglasCreateComponent } from './reglas-create.component';
import { ReglasEditarComponent } from './reglas-editar.component';

export const ReglasRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'lista',
            component: ReglasListComponent
        }]
    },{
        path: '',
        children: [{
            path: 'crear',
            component: ReglasCreateComponent
        }]
    },{
        path: '',
        children: [{
            path: 'editar/:id',
            component: ReglasEditarComponent
        }]
    },
];