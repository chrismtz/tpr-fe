import { clientes } from "../clientes/clientes";
import { vale } from "../vales/vale";

export class bolsas {
    clienteId: number;
    estado: string;
    fechaAsignacion: string;
    fechaCaducidad: string;
    id: number;
    monto: number;
    puntajeAsignado: number;
    puntajeUtilizado: number;
    saldo: number;
}

export class JsonBolsas{
    data: DataBolsas = new DataBolsas();
}

export class DataBolsas{
    bolsas: bolsas[];
    message: string;
    status: number;
}

export class Use{
    clienteId: number
    conceptoId: number
}

export class Json{
    data: Data
    message: string
    status: number
}

export class Data{
    bolsas : Bolsa
}

export class Bolsa{
    id: number
    puntajeUtilizado: number
    fecha: string
    usoDetalle: datos[]
    vale: vale
}

export class datos{
    id: number;
    puntajeUtilizado: number;
    bolsaPuntos: BolsaPuntos
}
export class BolsaPuntos{
    cliente: clientes;
    estado: string;
    fechaAsignacion: string;
    fechaCaducidad: string;
    id: number;
    monto: number;
    puntajeAsignado: number;
    puntajeUtilizado: number;
    saldo: number;
}

export class JsonEquivalencia{
    data: Punto
    status: number
    message: string
}

export class Punto{
    punto: number
}