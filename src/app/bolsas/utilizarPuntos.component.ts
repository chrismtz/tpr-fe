import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { BolsasServices } from './bolsas.services';
import { clientes } from '../clientes/clientes';
import { vale } from '../vales/vale';
import { Use } from './bolsas';


@Component({
    selector: 'app-utilizar-puntos',
    templateUrl: './utilizarPuntos.component.html'
  })

export class UtilizarPuntosComponent implements OnInit{
    clientes: clientes[] = []
    vales: vale[] = []
    cliente = new clientes()
    vale = new vale()
    use = new Use()
    submitted = false;
    hasError = false
    errorMessage = ""

    constructor(private bolsasService: BolsasServices, private router: Router ) { }

    ngOnInit(){
        this.use.clienteId = -1
        this.use.conceptoId = -1
        this.vale.id = -1
        this.cliente.id = -1
        this.vales.push(this.vale);
        this.clientes.push(this.cliente)
        this.bolsasService.getAllCliente().subscribe(
            data =>{
                this.clientes = this.clientes.concat(data.data.clientes)
            },
            error =>{
                console.error(error)
            }
        )
        this.bolsasService.getAllVales().subscribe(
            data =>{
                this.vales = this.vales.concat(data.data.vales)
            },
            error =>{
                console.error(error)
            }
        )
    }

    onSubmit(){
        this.hasError = false
        this.submitted = false
        if(this.use.clienteId < 0 || this.use.conceptoId < 0){
            console.log("Si")
            this.hasError = true
            this.errorMessage = "Falta Parametros"
        }
        console.log(this.use)

        if(!this.hasError){
            console.log(this.use)
            this.bolsasService.utilizarPunto(this.use).subscribe(
                data=>{
                    console.log(data)
                    if(data.status > 0){
                        this.hasError = true
                        this.errorMessage = data.message
                    }else{
                        this.submitted = true
                        this.cliente = new clientes()
                        this.cliente.id = -1
                        this.vale = new vale()
                        this.vale.id = -1
                        this.use = new Use()
                        this.use.clienteId = -1
                        this.use.conceptoId = -1
                    }
                }
            )
        }
    }

    isSubmitted(){
        if(this.submitted){
            return true;
        }else{
            return false;
        }
      }
}