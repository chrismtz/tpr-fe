import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { BolsasServices } from './bolsas.services';

@Component({
    selector: 'app-equivalencia',
    templateUrl: './equivalencia.component.html'
  })

export class EquivalenciaComponent implements OnInit{
    monto: number
    submitted = false;
    hasError = false
    errorMessage = ""
    message = ""

    constructor(private bolsasService: BolsasServices, private router: Router ) { }

    ngOnInit(){

    }

    onSubmit(){
        this.hasError = false
        this.submitted = false
        console.log(this.monto)
        if(this.monto){
            this.bolsasService.getEquivalencia(this.monto).subscribe(
                data=>{
                    console.log(data)
                    if(data.status>0){
                        this.errorMessage = data.message
                        this.hasError = true
                    }else{
                        var punto = data.data.punto
                        this.submitted = true;
                        this.message = `La cantidad de puntos para ${this.monto} es: ${punto}`
                    }
                },
                error => {
                    this.errorMessage = error
                    this.hasError = true
                }
            )
        }else{
            this.errorMessage = "Falta Paramentros"
            this.hasError = true
        }
    }

    addPuntos(){
        this.router.navigate(['bolsas/add']);
    }
}