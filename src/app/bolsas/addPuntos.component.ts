import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

import { clientes } from '../clientes/clientes';
import { bolsas } from '../bolsas/bolsas';
import { BolsasServices } from '../bolsas/bolsas.services';

export class ClienteStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-addPuntos',
  templateUrl: './addPuntos.component.html'
})
export class AddPuntosComponent implements OnInit {
  @ViewChild('content', {static: false}) content: ElementRef;

  consultas: bolsas[];
  bolsa: bolsas = new bolsas();
  term : Object = {};
  clientes: clientes[];
  clienteId: number;
  mensaje;
  error: boolean = false;
  monto: number;

  submitted = false;
  validClienteType: boolean = false;
  validMontoType: boolean = false;

  type : FormGroup;

  constructor(private bolsasService: BolsasServices, private router: Router, private formBuilder: FormBuilder ) { }

  ngOnInit() {
    this.type = this.formBuilder.group({
      // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      clienteId: [null, Validators.required],
      monto: [null, Validators.required]
    });
  }

  validateAllFormFields(formGroup: FormGroup){
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if(control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
        }else if ( control instanceof FormGroup) {
            this.validateAllFormFields(control);
        }
    });
  }

  isFieldValid(form: FormGroup, field: string){
    return !form.get(field).valid && form.get(field).touched;
  }

  onType(){
    if (this.type.valid){

    }else{
        this.validateAllFormFields(this.type);
    }
  }

  displayFieldCss(form: FormGroup, field: string){
    return {
        'has-error': this.isFieldValid(form,field),
        'has-feedback': this.isFieldValid(form,field)
    };
  }

  clienteValidationType(e) {
    if (e) {
        this.validClienteType = true;
    }else {
        this.validClienteType = false;
    }
  }

  montoValidationType(e) {
    if (e) {
        this.validMontoType = true;
    }else {
        this.validMontoType = false;
    }
  }

  isError(){
      if(this.error){
        return true;
      }else{
        return false;
      }
  }

  save() {
    /*acá modificar si es necesario para ingresar más datos*/
    console.log('bolsa', this.bolsa);
    //console.log('nacimineot', this.cliente.fechaNacimiento);
    this.bolsasService.crearBolsa(this.bolsa).subscribe(
        data => {
            console.log(data);
            this.mensaje = "Se ha agregado los puntos correctamente.";
        },
        error => {
            console.log(error)
            this.mensaje = "No se ha podido agregar los puntos.";
            this.error = true;
          }
    
    );
    this.bolsa = new bolsas();
  }

  onSubmit() {
    if (this.type.valid){
        this.submitted = true;
        this.bolsa = this.type.value;
        console.log(this.type);
        console.log(this.bolsa);
        console.log(this.submitted);
        this.save();
    }else{
        this.validateAllFormFields(this.type);
    }
  }

  isSubmitted(){
    if(this.submitted){
        return true;
    }else{
        return false;
    }
  }

  goequivalencia(){
    this.router.navigate(['bolsas/equivalencia']);
  }

}
