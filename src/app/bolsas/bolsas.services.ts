import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {JsonBolsas, bolsas, Json, Use, JsonEquivalencia} from './bolsas';
import { JsonClientes } from '../clientes/clientes';
import { JsonVales } from '../vales/vale';

@Injectable()

export class BolsasServices {
    private api = 'http://gy7228.myfoscam.org:8080/rest/bolsas/';
    private apiClientes = 'http://gy7228.myfoscam.org:8080/rest/clientes/';
    private apiVale = 'http://gy7228.myfoscam.org:8080/rest/vales/';
    private apiEquivalencia = 'http://gy7228.myfoscam.org:8080/rest/reglas/equivalencia'

    constructor( private http: HttpClient ) { }

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };

    crearBolsa(param: Object): Observable<Object> {
        const queryParams = `add`;
        return this.http.post<JsonBolsas>(`${this.api}${queryParams}`, param, this.httpOptions);
    }

    getAllCliente(): Observable<JsonClientes>{
        const queryParams = `all`;
        return this.http.get<JsonClientes>(`${this.apiClientes}${queryParams}`);
    }

    getAllVales(): Observable<JsonVales>{
        const queryParams = `all`;
        return this.http.get<JsonVales>(`${this.apiVale}${queryParams}`);
    }

    utilizarPunto(param : Use): Observable<Json>{
        return this.http.put<Json>(`${this.api}use`, param)
    }

    getEquivalencia(monto:number) : Observable<JsonEquivalencia>{
        return this.http.get<JsonEquivalencia>(`${this.apiEquivalencia}/${monto}`)
    }
        
}