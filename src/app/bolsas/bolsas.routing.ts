import {Routes} from '@angular/router';
import { AddPuntosComponent } from './addPuntos.component';
import { UtilizarPuntosComponent } from './utilizarPuntos.component';
import { EquivalenciaComponent } from './equivalencia.component';

export const BolsasRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'add',
            component: AddPuntosComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'utilizar',
            component: UtilizarPuntosComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'equivalencia',
            component: EquivalenciaComponent
        }]
    }
];