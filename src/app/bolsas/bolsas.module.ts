import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { MdModule } from '../md/md.module';
import { BolsasRoutes } from './bolsas.routing';
import { HttpClientModule } from '@angular/common/http';
import { AddPuntosComponent } from './addPuntos.component';
//import { ClientesCreateComponent } from './clientes-create.component';
import { BolsasServices } from './bolsas.services';
import { TagInputModule } from 'ngx-chips';
import { MensajeDisplayModule } from '../shared/mensaje-display/mensaje-display.module';
import { UtilizarPuntosComponent } from './utilizarPuntos.component';
import { EquivalenciaComponent } from './equivalencia.component'

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(BolsasRoutes),
        FormsModule,
        MdModule,
        HttpClientModule,
        MaterialModule,
        ReactiveFormsModule,
        TagInputModule,
        MensajeDisplayModule
    ],
    declarations: [
        AddPuntosComponent,
        UtilizarPuntosComponent,
        EquivalenciaComponent
        //ClientesCreateComponent
    ],
    providers: [ BolsasServices ]
})
export class BolsasModule { }
