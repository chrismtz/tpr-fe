import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { MdModule } from '../md/md.module';
import { HttpClientModule } from '@angular/common/http';
import { ParametrosService } from './parametros.service';
import { ParamentrosRoutes } from './parametros.routing';
import { ParametrosListComponent } from './parametros-list.component';
import { ParametrosCreateComponent } from './parametros-create.component';
import { ParametrosUpdateComponent } from './parametros-editar.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ParamentrosRoutes),
        FormsModule,
        MdModule,
        HttpClientModule,
        MaterialModule,
        ReactiveFormsModule,
    ],
    declarations: [
        ParametrosListComponent,
        ParametrosCreateComponent,
        ParametrosUpdateComponent
    ],
    providers: [ ParametrosService ]
})

export class ParametrosModule {}