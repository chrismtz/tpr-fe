import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Parametro } from './parametros';
import { ParametrosService } from './parametros.service';


@Component({
    selector: 'app-parametros-lis',
    templateUrl: './parametros-list.component.html'
})

export class ParametrosListComponent implements OnInit{
    parametros: Parametro[]
    total: number;
    cantidadPagina = 5;
    paginas: number[] = [];
    actual = 0;
    hasError = false
    errorMessage = ""

    constructor(private parametroService: ParametrosService, private router: Router){}

    ngOnInit(){
        this.loadData()
    }

    loadData(page: number = 0){
        this.parametroService.listarParametros(page, this.cantidadPagina).subscribe(
            data => {
                console.log(data.message)
                var datos = data.data
                this.parametros = datos.parametros
                this.total = this.parametros.length
                let page_size = 1
                this.paginas = new Array(page_size);
                console.log(page_size)
                console.log(this.paginas)

            },
            error => {
                console.error(error)
                this.hasError = true
                this.errorMessage = error.message
            }
        )
    }

    pagesLoad(page: number){
        console.log(page)
        this.actual = page
        this.loadData(page)
    }


    isError(){
        if(this.hasError){
          return true;
        }else{
          return false;
        }
    }

    crearParametro(){
        this.router.navigate(['parametros/crear'])
    }

    editarParametro(id: number){
        this.router.navigate(['parametros/editar', id])
    }

}