import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Paginate, JsonParametro, Parametro } from './parametros';

@Injectable()

export class ParametrosService{
    private api = 'http://gy7228.myfoscam.org:8080/rest/parametros';


    constructor( private http: HttpClient ) { }

    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };

    listarParametros(page: number = 0, cant: number = 5): Observable<JsonParametro>{
        var body = new Paginate()
        body.pageSize = cant
        body.startIndexPage = page + 1
        return this.http.get<JsonParametro>(`${this.api}/all`)
    }

    crearParametro(parametros: Parametro): Observable<JsonParametro>{
        return this.http.post<JsonParametro>(`${this.api}/add`, parametros)
    }

    getParametro(id: number):Observable<Parametro>{
        return this.http.get<Parametro>(`${this.api}/id/${id}`)
    }

    editarParametro(parametros: Parametro): Observable<JsonParametro>{
        return this.http.put<JsonParametro>(`${this.api}/edit/${parametros.id}`, parametros)
    }
    
}