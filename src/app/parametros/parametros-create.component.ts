import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParametrosService } from './parametros.service';
import { Parametro } from './parametros';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'app-parametros-create',
    templateUrl: './parametros-create.component.html'
})

export class ParametrosCreateComponent implements OnInit{
    parametro = new Parametro()
    submitted = false
    hasError = false
    errorMessage = ""
    formGroup: FormGroup

    constructor(private parametrosService: ParametrosService, private router: Router){}

    ngOnInit(){
        this.formGroup = new FormGroup({
            fechaInicioValidez: new FormControl(''),
            fechaFinValidez: new FormControl(''),
            diasDuracion: new FormControl('')
        })
    }

    onSubmit(){
        this.submitted = false
        this.hasError = false
        if(!this.parametro.fechaInicioValidez || !this.parametro.fechaFinValidez || !this.parametro.diasDuracion){
            this.hasError = true
            this.errorMessage = "Faltan parametros"
        }
        console.log(this.formGroup.valid)
        if(!this.hasError){
            // this.regla = this.formGroup.value
            console.log(this.parametro)
            this.parametrosService.crearParametro(this.parametro).subscribe(
                data => {
                    console.log(data)
                    var dato = data.data
                    this.submitted = true
                    this.parametro = new Parametro()
                },
                error => {
                    console.log(error.message)
                    this.hasError = true
                    this.errorMessage = error.message
                }
            )
        }
    }

    gotoList() {
        this.router.navigate(['parametros/lista']);
    }

}