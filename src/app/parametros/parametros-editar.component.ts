import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Parametro } from './parametros';
import { ParametrosService } from './parametros.service';

@Component({
    selector: 'app-parametros-edit',
    templateUrl: './parametros-create.component.html'
})

export class ParametrosUpdateComponent implements OnInit{
    parametro = new Parametro()
    id: number
    submitted = false
    hasError = false
    errorMessage = ""
    formGroup: FormGroup

    constructor(private parametroService: ParametrosService, private router: Router, private route: ActivatedRoute){}

    ngOnInit(){
        this.formGroup = new FormGroup({
            fechaInicioValidez: new FormControl(''),
            fechaFinValidez: new FormControl(''),
            diasDuracion: new FormControl('')
        })
        this.id = +this.route.snapshot.paramMap.get('id')
        this.parametroService.getParametro(this.id).subscribe(
            data =>{
                console.log(data)
                // var inicio = new Date(data.fechaInicioValidez as string)
                // var fin = new Date(data.fechaFinValidez as string)
                // this.parametro.diasDuracion = data.diasDuracion
                // this.parametro.fechaInicioValidez = `${inicio.getUTCDay()}-${inicio.getUTCMonth()}-${inicio.getUTCFullYear()}`
                // this.parametro.fechaFinValidez = `${fin.getUTCDay()}-${fin.getUTCMonth()}-${fin.getUTCFullYear()}`
                this.parametro = data
            },
            error =>{
                console.log(error)
                this.hasError = true
                this.errorMessage = error.message
            }
        )
    }

    onSubmit(){
        this.submitted = false
        this.hasError = false
        if(!this.parametro.fechaInicioValidez || !this.parametro.fechaFinValidez || !this.parametro.diasDuracion){
            this.hasError = true
            this.errorMessage = "Faltan parametros"
        }
        console.log(this.formGroup.valid)
        if(!this.hasError){
            // this.regla = this.formGroup.value
            console.log(this.parametro)
            this.parametroService.editarParametro(this.parametro).subscribe(
                data => {
                    console.log(data)
                    this.submitted = true
                    this.parametro = new Parametro()
                    this.gotoList()
                },
                error => {
                    console.log(error.message)
                    this.hasError = true
                    this.errorMessage = error.message
                }
            )
        }
    }

    gotoList() {
        this.router.navigate(['parametros/lista']);
    }
}