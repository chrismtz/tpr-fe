export class Parametro{
    id: number
    fechaInicioValidez: String
    fechaFinValidez: String
    diasDuracion: number
}

export class Data{
    parametros: Parametro[]
}

export class JsonParametro{
    data: Data;
    status: number;
    message: string;
}

export class Paginate{
    startIndexPage: number
    pageSize: number
}
