import {Routes} from '@angular/router';
import { ParametrosListComponent } from './parametros-list.component';
import { ParametrosCreateComponent } from './parametros-create.component';
import { ParametrosUpdateComponent } from './parametros-editar.component';

export const ParamentrosRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'lista',
            component: ParametrosListComponent
        }]
    },{
        path: '',
        children: [{
            path: 'crear',
            component: ParametrosCreateComponent
        }]
    },{
        path: '',
        children: [{
            path: 'editar/:id',
            component: ParametrosUpdateComponent
        }]
    },
]