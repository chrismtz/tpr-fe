import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-msg-display',
  templateUrl: './mensaje-display.component.html',
  styleUrls: ['./mensaje-display.component.css']
})
export class MensajeDisplayComponent {

  @Input() errorMsg: string;
  @Input() displayError: boolean;
  @Input() Msg: string;
  @Input() displayMsg: boolean;

}
