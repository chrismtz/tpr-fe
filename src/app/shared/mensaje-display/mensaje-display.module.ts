import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MensajeDisplayComponent } from './mensaje-display.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MensajeDisplayComponent],
  exports: [MensajeDisplayComponent]
})
export class MensajeDisplayModule { }
