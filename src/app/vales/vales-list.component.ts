import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { vale } from './vale';
import { ValesService } from './vales.services';
import { pagina } from '../paginate/pagina';

@Component({
    selector: 'app-vales-lis',
    templateUrl: './vales-list.component.html'
})

export class ValesListComponent implements OnInit {
    vales: vale[];
    vales2: vale[];
    vale: vale = new vale();
    term = '';
    //searched = false;
    startIndexPage: number = 1;
    pageSize: number = 5;
    mensaje;
    error: boolean = false;
    prevbtn: boolean = true;
    nextbtn: boolean = true;

    constructor( private valesService: ValesService , private router: Router ) {}

    ngOnInit() {
        this.loadData();
    }

    loadData() {
        
        this.prevbtn = false;
        this.valesService.listaVales(this.startIndexPage, this.pageSize).subscribe(
            data => {
                console.log(data.data.vales);
                this.vales = data.data.vales;
                console.log(this.vales);
                this.siguientesvales();
            }
            ,

            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de vales";
                this.error = true;
            }
        );
    }

    /*search() {
        this.pagina.pageSize = this.pageSize;
        this.pagina.startIndexPage = this.startIndexPage;
        this.prevbtn = false;
        this.valesService.searchVale(this.term,this.pagina).subscribe(
            data => {
                this.vales = data.data.vales;
                this.searched = true;
                console.log(this.vales);
                if (this.vales.length < 5){
                    this.nextbtn = false;
                }
            },
            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de vales";
                this.error = true;
            }
        );
    }

    onSubmit() {
        if ( this.term ) {

        } else {
            this.term = '';
        }
        console.log(this.term);
        this.search();
    }*/

    nextpage() {
        //this.actual = page;
        this.startIndexPage = this.startIndexPage + 1;
        this.prevbtn = true;
        this.valesService.listaVales(this.startIndexPage, this.pageSize).subscribe(
            data => {
                console.log(data);
                this.vales = data.data.vales;
                console.log(this.vales);
                this.siguientesvales();
            },

            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de vales";
                this.error = true;
            }
        );
    }

    prevpage() {
        //this.actual = page;
        this.startIndexPage = this.startIndexPage - 1;
        this.nextbtn = true;
        if (this.startIndexPage == 1) {
            this.prevbtn = false;
        }
        
        this.valesService.listaVales(this.startIndexPage, this.pageSize).subscribe(
            data => {
                console.log(data);
                this.vales = data.data.vales;
                console.log(this.vales);
            },

            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de vales";
                this.error = true;
            }
        );
    }

    siguientesvales(){
        this.valesService.listaVales(this.startIndexPage + 1, this.pageSize).subscribe(
            data => {
                console.log(data);
                this.vales2 = data.data.vales;
                console.log(this.vales2);
                if( this.vales2 === null ){
                    console.log('Hola');
                    this.nextbtn = false;
                }
            },

            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de vales";
                this.error = true;
            }
        );
    }

    isError(){
        if(this.error){
          return true;
        }else{
          return false;
        }
    }

    createVale(){
        this.router.navigate(['vales/create']);
    }
    
}
