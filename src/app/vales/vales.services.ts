import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { JsonVales, vale } from './vale';


@Injectable()

export class ValesService {
    private api = 'http://gy7228.myfoscam.org:8080/rest/vales/';

    constructor( private http: HttpClient ) { }

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };

    listaVales( page: number = 1, size:number = 5) : Observable<JsonVales>{
        const queryParams = `paginado?page=${page}&size=${size}`;
        return this.http.get<JsonVales>(`${this.api}${queryParams}`, this.httpOptions);
    }

    listaTodosVales(): Observable<JsonVales> {
        const queryParams = `all`;
        return this.http.get<JsonVales>(`${this.api}${queryParams}`);
    }

    crearVale(vale: vale): Observable<Object> {
        const queryParams = `add`;
        return this.http.post(`${this.api}${queryParams}`, vale, this.httpOptions);
    }

    getVale(id: number): Observable<any> {
        return this.http.get(`${this.api}/${id}`);
    }

    /*searchVale(term: string, paginate: Object){
        console.log(term);
        const queryParams = `getByPage?parametro=${term}`;
        return this.http.post<JsonVales>(`${this.api}${queryParams}`,paginate, this.httpOptions);
    }*/

}
