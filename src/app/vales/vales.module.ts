import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { MdModule } from '../md/md.module';
import { HttpClientModule } from '@angular/common/http';
import { TagInputModule } from 'ngx-chips';
import { ValesRoutes } from './vales.routing';
import { ValesService } from './vales.services';
import { ValesListComponent } from './vales-list.component';
import { ValesCreateComponent } from './vales-create.component';
import { MensajeDisplayModule } from '../shared/mensaje-display/mensaje-display.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ValesRoutes),
        FormsModule,
        MdModule,
        HttpClientModule,
        MaterialModule,
        ReactiveFormsModule,
        TagInputModule,
        MensajeDisplayModule
    ],
    declarations: [
        ValesListComponent,
        ValesCreateComponent
    ],
    providers: [ ValesService ]
})

export class ValesModule {}
