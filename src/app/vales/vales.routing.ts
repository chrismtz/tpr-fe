import {Routes} from '@angular/router';
import { ValesListComponent } from './vales-list.component';
import { ValesCreateComponent } from './vales-create.component';

export const ValesRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'list',
            component: ValesListComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'create',
            component: ValesCreateComponent
        }]
    }
];
