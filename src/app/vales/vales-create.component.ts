import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MyErrorStateMatcher } from '../forms/validationforms/validationforms.component';
import { vale } from './vale';
import { ValesService } from './vales.services';


export class ValeStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
  }
  

@Component({
    selector: 'app-vales-cre',
    templateUrl: './vales-create.component.html'
})

export class ValesCreateComponent implements OnInit {
    vale: vale = new vale();
    submitted = false;
    mensaje;
    error: boolean = false;

    emailFormControl = new FormControl('', [
        Validators.required
    ]);

    validDescripcionType: boolean = false;
    validCantidadType: boolean = false;

    matcher = new ValeStateMatcher();
    type : FormGroup;


    constructor( private valesService: ValesService, private router: Router, private formBuilder: FormBuilder) {

    }

    ngOnInit() {
        this.type = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            descripcion: [null, Validators.required],
            cantidadRequerida: [null, Validators.required]
        });
    }


    isFieldValid(form: FormGroup, field: string){
        return !form.get(field).valid && form.get(field).touched;
    }

    displayFieldCss(form: FormGroup, field: string){
        return {
            'has-error': this.isFieldValid(form,field),
            'has-feedback': this.isFieldValid(form,field)
        };
    }

    onType(){
        if (this.type.valid){

        }else{
            this.validateAllFormFields(this.type);
        }
    }

    validateAllFormFields(formGroup: FormGroup){
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if(control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }else if ( control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    descripcionValidationType(e){
        if (e) {
            this.validDescripcionType = true;
        }else {
            this.validDescripcionType = false;
        }
    }

    cantidadValidationType(e) {
        if (e) {
            this.validCantidadType = true;
        }else {
            this.validCantidadType = false;
        }
    }
    

    save() {
        /*acá modificar si es necesario para ingresar más datos*/
        console.log('vale', this.vale);
        this.valesService.crearVale(this.vale).subscribe(
            data => {
                console.log(data);
                this.mensaje = "Se ha agregado el vale correctamente.";
            },
            error => {
                console.log(error)
                this.mensaje = "No se ha podido agregar el nuevo vale.";
                this.error = true;
              }
        
        );
        this.vale = new vale();
    }

    onSubmit() {
        if (this.type.valid){
            this.submitted = true;
            this.vale = this.type.value;
            console.log(this.type);
            console.log(this.vale);
            console.log(this.submitted);
            this.save();
        }else{
            this.validateAllFormFields(this.type);
        }
    }

    isSubmitted(){
        if(this.submitted){
            return true;
        }else{
            return false;
        }
    }

    gotoList() {
        this.router.navigate(['vales/list']);
    }
}
