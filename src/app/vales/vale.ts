export class vale {
    id: number;
    descripcion: string;
    cantidadRequerida: number;
}

export class JsonVales {
    data: DataVales = new DataVales();
}

export class DataVales {
    vales: vale[];
    status: number;
    message: string;
}