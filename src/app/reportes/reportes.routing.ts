import {Routes} from '@angular/router';
import { ReporteUsoPuntosComponent } from './reporte-usopuntos.component';
import { ReporteBolsaPuntosComponent } from './reporte-bolsapuntos.component';
import { ReporteVencimientosComponent } from './reporte-vencimientos.component';

export const ReportesRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'usoPuntos',
            component: ReporteUsoPuntosComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'bolsaPuntos',
            component: ReporteBolsaPuntosComponent
        }]
    }, {
        path: '',
        children: [{
            path: 'vencimientos',
            component: ReporteVencimientosComponent
        }]
    }
];