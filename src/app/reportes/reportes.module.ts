import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { MdModule } from '../md/md.module';
import { HttpClientModule } from '@angular/common/http';
import { TagInputModule } from 'ngx-chips';
import { MensajeDisplayModule } from '../shared/mensaje-display/mensaje-display.module';
import { ReportesRoutes } from './reportes.routing';
import { ReportesService } from './reportes.services';
import { ReporteUsoPuntosComponent } from './reporte-usopuntos.component';
import { ReporteBolsaPuntosComponent } from './reporte-bolsapuntos.component';
import {  ReporteVencimientosComponent } from './reporte-vencimientos.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ReportesRoutes),
        FormsModule,
        MdModule,
        HttpClientModule,
        MaterialModule,
        ReactiveFormsModule,
        TagInputModule,
        MensajeDisplayModule
    ],
    declarations: [
        ReporteUsoPuntosComponent,
        ReporteBolsaPuntosComponent,
        ReporteVencimientosComponent
    ],
    providers: [ ReportesService ]
})

export class ReportesModule {}
