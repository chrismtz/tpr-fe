import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { JsonUsoPuntos, JsonBolsaPuntos, JsonVencimientos } from './reportes';


@Injectable()

export class ReportesService {
    private api = 'http://gy7228.myfoscam.org:8080/rest/consultas/';

    constructor( private http: HttpClient ) { }

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };

    listaUsosPuntos( param: Object){
        const queryParams = `usoPuntos`;
        return this.http.post<JsonUsoPuntos>(`${this.api}${queryParams}`,param, this.httpOptions);
    }

    listaBolsasPuntos( param: Object ) {
        const queryParams = `bolsaPuntos`;
        return this.http.post<JsonBolsaPuntos>(`${this.api}${queryParams}`, param, this.httpOptions);
    }

    listaVencimientos( param: Object ) {
        const queryParams = `vencimientos`;
        return this.http.post<JsonVencimientos>(`${this.api}${queryParams}`, param, this.httpOptions);
    }

}
