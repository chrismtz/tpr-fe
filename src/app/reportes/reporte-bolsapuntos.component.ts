import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ReportesService } from './reportes.services';
import { bolsaPuntos } from './reportes';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import * as XLSX from 'xlsx';
import { ClientesService } from '../clientes/clientes.services';
import { clientes } from '../clientes/clientes';
import { vale } from '../vales/vale';
import { ValesService } from '../vales/vales.services';

@Component({
    selector: 'app-reporte-bp',
    templateUrl: './reporte-bolsapuntos.component.html'
})

export class ReporteBolsaPuntosComponent implements OnInit {
    @ViewChild('content', {static: false}) content: ElementRef;

    consultas: bolsaPuntos[];
    term : Object = {};
    clientes: clientes[];
    vales: vale[];
    searched = false;
    mensaje;
    error: boolean = false;
    clienteId: number;

    constructor( private reportesService: ReportesService , private router: Router, 
        private clientesService: ClientesService ) {}

    ngOnInit() {
        this.loadData();
    }

    loadData() {
        this.reportesService.listaBolsasPuntos(this.term).subscribe(
            data => {
                console.log(data);
                this.consultas = data.data.bolsaPuntos;
                console.log(this.consultas);
            }
            ,

            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de uso de puntos";
                this.error = true;
            }
        );
        this.clientesService.listaTodosClientes().subscribe(
            data => {
                console.log(data);
                this.clientes = data.data.clientes;
                console.log(this.clientes)
            },
            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de clientes";
                this.error = true;
            }
        );
    }

    search() {
        this.reportesService.listaBolsasPuntos(this.term).subscribe(
            data => {
                this.consultas = data.data.bolsaPuntos;
                this.searched = true;
                console.log(this.consultas);
            },
            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de uso de puntos";
                this.error = true;
            }
        );
    }

    onSubmit() {
        if ( this.clienteId ) {
            this.term =JSON.stringify({
                clienteId: this.clienteId
            });
        } else {
            this.term = {};
        }
        console.log(this.term);
        this.search();
    }


    isError(){
        if(this.error){
          return true;
        }else{
          return false;
        }
    }

    pdf() {
        let doc = new jsPDF();

        doc.autoTable({
            html: '#mytable'
        });

        doc.save('Reporte_Bolsa_Puntos.pdf');
    }

    excel() {
        let element = document.getElementById('mytable');
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        XLSX.writeFile(wb, 'Reporte_Bolsa_Puntos.xlsx');
    }

    clear() {
        this.clienteId = undefined;
    }
    
    gousopuntos(){
        this.router.navigate(['reportes/usoPuntos']);
    }

    govencimientos(){
        this.router.navigate(['reportes/vencimientos']);
    }
}
