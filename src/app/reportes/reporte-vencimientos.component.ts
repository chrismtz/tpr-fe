import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ReportesService } from './reportes.services';
import { vencimientos } from './reportes';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import * as XLSX from 'xlsx';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class VenStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
  }


@Component({
    selector: 'app-reporte-vto',
    templateUrl: './reporte-vencimientos.component.html'
})

export class ReporteVencimientosComponent implements OnInit {
    @ViewChild('content', {static: false}) content: ElementRef;

    consultas: vencimientos[];
    term : Object = {};
    searched = false;
    mensaje;
    error: boolean = false;

    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email,
    ]);

    validDiasType: boolean = false;

    matcher = new VenStateMatcher();
    type : FormGroup;


    constructor( private reportesService: ReportesService , private router: Router, private formBuilder: FormBuilder ) {}

    ngOnInit() {
        this.type = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            dias: [null, Validators.required],
        });

        this.loadData();
    }

    loadData() {
        this.term = JSON.stringify({
            dias: 50
        });
        this.reportesService.listaVencimientos(this.term).subscribe(
            data => {
                console.log(data);
                this.consultas = data.data.clientes;
                console.log(this.consultas);
            }
            ,

            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de vencimientos";
                this.error = true;
            }
        );
    }

    search() {
        this.reportesService.listaVencimientos(this.term).subscribe(
            data => {
                this.consultas = data.data.clientes;
                this.searched = true;
                console.log(this.consultas);
            },
            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de uso de puntos";
                this.error = true;
            }
        );
    }

    onSubmit() {
        if (this.type.valid){
            this.term = this.type.value;
            console.log(this.term);
            this.search();
        }else{
            this.validateAllFormFields(this.type);
        }
    }


    isError(){
        if(this.error){
          return true;
        }else{
          return false;
        }
    }

    pdf() {
        let doc = new jsPDF();

        doc.autoTable({
            html: '#mytable'
        });

        doc.save('Reporte_Vencimientos.pdf');
    }

    excel() {
        let element = document.getElementById('mytable');
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        XLSX.writeFile(wb, 'Reporte_Vencimientos.xlsx');
    }

    clear() {
        this.type.reset();
    }

    isFieldValid(form: FormGroup, field: string){
        return !form.get(field).valid && form.get(field).touched;
    }

    displayFieldCss(form: FormGroup, field: string){
        return {
            'has-error': this.isFieldValid(form,field),
            'has-feedback': this.isFieldValid(form,field)
        };
    }

    onType(){
        if (this.type.valid){

        }else{
            this.validateAllFormFields(this.type);
        }
    }

    validateAllFormFields(formGroup: FormGroup){
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if(control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }else if ( control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    diasValidationType(e){
        if (e) {
            this.validDiasType = true;
        }else {
            this.validDiasType = false;
        }
    }

    gousopuntos(){
        this.router.navigate(['reportes/usoPuntos']);
    }

    gobolsapuntos(){
        this.router.navigate(['reportes/bolsaPuntos']);
    }

}
