import { vale } from "../vales/vale";
import { clientes } from "../clientes/clientes";

export class UsoPuntos {
    fecha: string;
    id: number;
    puntajeUtilizado: number;
    usoDetalle: usoDetalle[];
    vale: vale = new vale();
}

export class usoDetalle {
    bolsaPuntos: bolsaPuntos = new bolsaPuntos();
    id: number;
    puntajeUtilizado: number;
}

export class bolsaPuntos {
    cliente: clientes = new clientes();
    estado: string;
    fechaAsignacion: string;
    fechaCaducidad: string;
    id: number;
    monto: number;
    puntajeAsignado: number;
    puntajeUtilizado: number;
    saldo: number;
}

export class JsonUsoPuntos {
    data: DataUsoPuntos = new DataUsoPuntos();
    status: number;
    message: string;
}

export class DataUsoPuntos {
    cabeceras: UsoPuntos[];
}

export class JsonBolsaPuntos {
    data: DataBolsaPuntos = new DataBolsaPuntos();
    status: number;
    message: string;
}

export class DataBolsaPuntos {
    bolsaPuntos: bolsaPuntos[];
}

export class JsonVencimientos {
    data: DataVencimientos = new DataVencimientos();
    status: number;
    message: string;
}

export class DataVencimientos {
    clientes: vencimientos[];
}

export class vencimientos {
    clienteId: number;
    nombre: string;
    apellido: string;
    nroDocumento: string;
    fechaCaducidad: string;
    bolsaId: number;
}