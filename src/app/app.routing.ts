import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [
{
      path: '',
      redirectTo: 'inicio',
      pathMatch: 'full',
    }, {
      path: '',
      component: AdminLayoutComponent,
      children: [
          {
        path: '',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    }, {
          path: 'clientes',
          loadChildren: './clientes/clientes.module#ClientesModule'
    }, {
        path: 'vales',
        loadChildren: './vales/vales.module#ValesModule'
    }, {
        path: 'bolsas',
        loadChildren: './bolsas/bolsas.module#BolsasModule'
    }, {
        path: 'reportes',
        loadChildren: './reportes/reportes.module#ReportesModule'
    },{
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
    }, {
        path: 'forms',
        loadChildren: './forms/forms.module#Forms'
    }, {
        path: 'tables',
        loadChildren: './tables/tables.module#TablesModule'
    }, {
        path: 'maps',
        loadChildren: './maps/maps.module#MapsModule'
    }, {
        path: 'widgets',
        loadChildren: './widgets/widgets.module#WidgetsModule'
    }, {
        path: 'charts',
        loadChildren: './charts/charts.module#ChartsModule'
    }, {
        path: 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
    }, {
        path: '',
        loadChildren: './userpage/user.module#UserModule'
    }, {
        path: '',
        loadChildren: './timeline/timeline.module#TimelineModule'
    },{
        path: 'reglas',
        loadChildren: './reglas/reglas.module#ReglasModule'
    },{
        path: 'parametros',
        loadChildren: './parametros/parametros.module#ParametrosModule'
    }
  ]}, {
      path: '',
      component: AuthLayoutComponent,
      children: [{
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule'
      }]
    }
];
