export class clientes {
    id: number;
    nombre: string;
    apellido: string;
    nroDocumento: number;
    tipoDocumento: string;
    nacionalidad: string;
    email: string;
    telefono: number;
    fechaNacimiento: string | Date;
}

export class JsonClientes{
    data: DataClientes = new DataClientes();
}

export class DataClientes{
    clientes: clientes[];
    message: string;
    status: number;
}
