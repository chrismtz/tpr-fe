import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ClientesService } from './clientes.services';
import { clientes } from './clientes';
import { pagina } from '../paginate/pagina';
import { FormControl, FormGroupDirective, NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class CliStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}


@Component({
    selector: 'app-clientes-lis',
    templateUrl: './clientes-list.component.html'
})

export class ClientesListComponent implements OnInit {
    clientes: clientes[];
    clientes2: clientes[];
    cliente: clientes = new clientes();
    term : Object;
    param: string= '';
    searched = false;
    pagina: pagina = new pagina();
    nextpag: pagina = new pagina();
    startIndexPage: number = 0;
    pageSize: number = 5;
    mensaje;
    error: boolean = false;
    prevbtn: boolean = true;
    nextbtn: boolean = true;

    validSearchType: boolean = false;

    matcher = new CliStateMatcher();
    type : FormGroup;


    constructor( private clientesService: ClientesService , private router: Router, private formBuilder: FormBuilder ) {}

    ngOnInit() {
        this.type = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            search: [null, Validators.required],
        });

        this.loadData();
    }

    loadData() {
        this.pagina.pageSize = this.pageSize;
        this.pagina.startIndexPage = this.startIndexPage;
        this.prevbtn = false;
        this.nextbtn = true;
        this.clientesService.listaClientes(this.pagina).subscribe(
            data => {
                console.log(data.data.clientes);
                this.clientes = data.data.clientes;
                console.log(this.clientes);
                this.siguientesclientes();
            }
            ,

            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de clientes";
                this.error = true;
            }
        );
    }

    search() {
        this.pagina.pageSize = this.pageSize;
        this.pagina.startIndexPage = this.startIndexPage;
        this.prevbtn = false;
        this.nextbtn = true;
        this.clientesService.searchCliente(this.param,this.pagina).subscribe(
            data => {
                this.clientes = data.data.clientes;
                this.searched = true;
                console.log(this.clientes);
                this.siguientesclientes();
            },
            error => {
                console.log(error);
                this.mensaje = "No se pudo obtener la lista de clientes";
                this.error = true;
            }
        );
    }

    onSubmit() {
        if (this.type.valid){
            this.term = this.type.value;
            this.term = this.term.valueOf();
            this.term = Object.values(this.term);
            this.param = this.term[0];
            console.log(this.term);
            console.log(this.param);
            this.search();
        }else{
            this.validateAllFormFields(this.type);
        }

    }

    nextpage() {
        //this.actual = page;
        this.pagina.startIndexPage = this.pagina.startIndexPage + this.pagina.pageSize;
        this.prevbtn = true;
        this.nextbtn = true;
        if (this.searched) {
            this.clientesService.searchCliente(this.param, this.pagina).subscribe(
                data => {
                    console.log(data);
                    this.clientes = data.data.clientes;
                    console.log(this.clientes);
                    this.siguientesclientes();
                },

                error => {
                    console.log(error);
                    this.mensaje = "No se pudo obtener la lista de clientes";
                    this.error = true;
                }
            );
        } else {
            this.clientesService.listaClientes(this.pagina).subscribe(
                data => {
                    console.log(data);
                    this.clientes = data.data.clientes;
                    console.log(this.clientes);
                    this.siguientesclientes();
                },

                error => {
                    console.log(error);
                    this.mensaje = "No se pudo obtener la lista de clientes";
                    this.error = true;
                }
            );
        }
    }

    prevpage() {
        //this.actual = page;
        this.pagina.startIndexPage = this.pagina.startIndexPage - this.pagina.pageSize;
        this.nextbtn = true;
        this.prevbtn = true;
        if (this.pagina.startIndexPage <= 0) {
            this.prevbtn = false;
            this.pagina.startIndexPage = 0;
        }
        if (this.searched) {
            this.clientesService.searchCliente(this.param, this.pagina).subscribe(
                data => {
                    console.log(data);
                    this.clientes = data.data.clientes;
                    console.log(this.clientes);
                },

                error => {
                    console.log(error);
                    this.mensaje = "No se pudo obtener la lista de clientes";
                    this.error = true;
                }
            );
        } else {
            this.clientesService.listaClientes(this.pagina).subscribe(
                data => {
                    console.log(data);
                    this.clientes = data.data.clientes;
                    console.log(this.clientes);
                }
                ,

                error => {
                    console.log(error);
                    this.mensaje = "No se pudo obtener la lista de clientes";
                    this.error = true;
                }
            );
        }
    }

    isError(){
        if(this.error){
          return true;
        }else{
          return false;
        }
    }

    createCliente(){
        this.router.navigate(['clientes/create']);
    }
    
    siguientesclientes(){
        this.nextpag.startIndexPage = this.pagina.startIndexPage + this.pagina.pageSize;
        this.nextpag.pageSize = this.pagina.pageSize;
        if (this.searched) {
            this.clientesService.searchCliente(this.param, this.nextpag).subscribe(
                data => {
                    console.log(data);
                    this.clientes2 = data.data.clientes;
                    console.log(this.clientes2);
                    if( this.clientes2 === null ){
                        console.log('Hola');
                        this.nextbtn = false;
                    }

                },

                error => {
                    console.log(error);
                    this.mensaje = "No se pudo obtener la lista de clientes";
                    this.error = true;
                }
            );
        } else {
            this.clientesService.listaClientes(this.nextpag).subscribe(
                data => {
                    console.log(data);
                    this.clientes2 = data.data.clientes;
                    console.log(this.clientes2);
                    if( this.clientes2 === null ){
                        console.log('Hola');
                        this.nextbtn = false;
                    }
                }
                ,

                error => {
                    console.log(error);
                    this.mensaje = "No se pudo obtener la lista de clientes";
                    this.error = true;
                }
            );
        }
    }

    clear() {
        this.loadData();
        this.searched = false;
        this.type.reset();
        
    }

    isFieldValid(form: FormGroup, field: string){
        return !form.get(field).valid && form.get(field).touched;
    }

    displayFieldCss(form: FormGroup, field: string){
        return {
            'has-error': this.isFieldValid(form,field),
            'has-feedback': this.isFieldValid(form,field)
        };
    }

    onType(){
        if (this.type.valid){

        }else{
            this.validateAllFormFields(this.type);
        }
    }

    validateAllFormFields(formGroup: FormGroup){
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if(control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }else if ( control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    searchValidationType(e){
        if (e) {
            this.validSearchType = true;
        }else {
            this.validSearchType = false;
        }
    }


}
