import { Component, OnInit } from '@angular/core';
import { clientes } from './clientes';
import { ClientesService } from './clientes.services';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MyErrorStateMatcher } from '../forms/validationforms/validationforms.component';


export class ClienteStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
  }
  

@Component({
    selector: 'app-clientes-cre',
    templateUrl: './clientes-create.component.html'
})

export class ClientesCreateComponent implements OnInit {
    cliente: clientes = new clientes();
    fechaNacimiento: string;
    submitted = false;
    mensaje;
    error: boolean = false;

    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email,
    ]);

    validNombreType: boolean = false;
    validApellidoType: boolean = false;
    validEmailType: boolean = false;
    validNumberType: boolean = false;
    validCIType: boolean = false;
    validFecNacType: boolean = false;

    matcher = new ClienteStateMatcher();
    type : FormGroup;


    constructor( private clientesService: ClientesService, private router: Router, private formBuilder: FormBuilder) {

    }

    ngOnInit() {
        this.type = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            nombre: [null, Validators.required],
            apellido: [null, Validators.required],
            email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            telefono: [null, Validators.required],
            nroDocumento: [null , Validators.required],
            fechaNacimiento: [null , Validators.required],
        });
    }

    newCategoria() {
        this.submitted = true;
        this.save();
    }

    isFieldValid(form: FormGroup, field: string){
        return !form.get(field).valid && form.get(field).touched;
    }

    displayFieldCss(form: FormGroup, field: string){
        return {
            'has-error': this.isFieldValid(form,field),
            'has-feedback': this.isFieldValid(form,field)
        };
    }

    onType(){
        if (this.type.valid){

        }else{
            this.validateAllFormFields(this.type);
        }
    }

    validateAllFormFields(formGroup: FormGroup){
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if(control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }else if ( control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    nombreValidationType(e){
        if (e) {
            this.validNombreType = true;
        }else {
            this.validNombreType = false;
        }
    }

    apellidoValidationType(e) {
        if (e) {
            this.validApellidoType = true;
        }else {
            this.validApellidoType = false;
        }
    }

    emailValidationType(e){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(e).toLowerCase())) {
            this.validEmailType = true;
        } else {
          this.validEmailType = false;
        }
    }
    
    numberValidationType(e){
        if (e) {
            this.validNumberType = true;
        }else{
          this.validNumberType = false;
        }
    }
  
    ciValidationType(e) {
        if (e) {
            this.validCIType = true;
        }else {
            this.validCIType = false;
        }
    }

    fechanacValidationType(e) {
        if (e){
            this.validFecNacType = true;
        }else {
            this.validFecNacType = false;
        }
    }

    save() {
        /*acá modificar si es necesario para ingresar más datos*/
        console.log('cliente', this.cliente);
        console.log('nacimineot', this.cliente.fechaNacimiento);
        this.clientesService.crearCliente(this.cliente).subscribe(
            data => {
                console.log(data);
                this.mensaje = "Se ha agregado la categoria correctamente.";
            },
            error => {
                console.log(error)
                this.mensaje = "No se ha podido agregar la nueva categoría.";
                this.error = true;
              }
        
        );
        this.cliente = new clientes();
    }

    onSubmit() {
        if (this.type.valid){
            this.submitted = true;
            this.cliente = this.type.value;
            console.log(this.type);
            console.log(this.cliente);
            console.log(this.submitted);
            this.save();
        }else{
            this.validateAllFormFields(this.type);
        }
    }

    isSubmitted(){
        if(this.submitted){
            return true;
        }else{
            return false;
        }
    }

    gotoList() {
        this.router.navigate(['clientes/list']);
    }
}
