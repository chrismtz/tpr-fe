import {Routes} from '@angular/router';
import { ClientesListComponent } from './clientes-list.component';
import {ClientesCreateComponent} from './clientes-create.component';
/*import { PacientesDetailsComponent } from './pacientes-details.component';
import { PacientesUpdateComponent } from './pacientes-update.component';*/

export const ClientesRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'list',
            component: ClientesListComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'create',
            component: ClientesCreateComponent
        }]
    }/*{
        path: '',
        children: [ {
            path: 'details/:id',
            component: PacientesDetailsComponent
        }]
    },
    {
        path: '',
        children: [ {
            path: 'update/:id',
            component: PacientesUpdateComponent
        }]
    }*/
];
