import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { MdModule } from '../md/md.module';
import { ClientesRoutes } from './clientes.routing';
import { HttpClientModule } from '@angular/common/http';
import { ClientesListComponent } from './clientes-list.component';
import { ClientesCreateComponent } from './clientes-create.component';
import {ClientesService} from './clientes.services';
import { TagInputModule } from 'ngx-chips';
import { MensajeDisplayModule } from '../shared/mensaje-display/mensaje-display.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ClientesRoutes),
        FormsModule,
        MdModule,
        HttpClientModule,
        MaterialModule,
        ReactiveFormsModule,
        TagInputModule,
        MensajeDisplayModule
    ],
    declarations: [
        ClientesListComponent,
        ClientesCreateComponent
    ],
    providers: [ ClientesService ]
})

export class ClientesModule {}
