import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {JsonClientes, clientes} from './clientes';

@Injectable()

export class ClientesService {
    private api = 'http://gy7228.myfoscam.org:8080/rest/clientes/';

    constructor( private http: HttpClient ) { }

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };

    listaClientes( paginate: Object){
        const queryParams = `getByPage?`;
        return this.http.post<JsonClientes>(`${this.api}${queryParams}`,paginate, this.httpOptions);
    }

    listaTodosClientes(): Observable<JsonClientes> {
        const queryParams = `all`;
        return this.http.get<JsonClientes>(`${this.api}${queryParams}`);
    }

    crearCliente(cliente: Object): Observable<Object> {
        const queryParams = `add`;
        return this.http.post(`${this.api}${queryParams}`, cliente, this.httpOptions);
    }

  /*  eliminarCliente(id: number): Observable<any> {
        return this.http.delete(`${this.api}/${id}`, this.httpOptions);
    }*/

    getCliente(id: number): Observable<any> {
        return this.http.get(`${this.api}/${id}`);
    }

  /*  updateCliente(id: number, cliente: Object): Observable<Object> {
        return this.http.put(`${this.api}`, cliente, this.httpOptions);
    }*/

    searchCliente(term: string, paginate: Object){
        console.log(term);
        const queryParams = `getByPage?parametro=${term}`;
        return this.http.post<JsonClientes>(`${this.api}${queryParams}`,paginate, this.httpOptions);
    }

}
